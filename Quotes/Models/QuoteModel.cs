﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace Quotes.Models
{
    public class QuoteModel
    {

        public int QuoteId { get; set; }

        [Required]
        [Display(Name="Quote")]
        public string QuoteText { get; set; }

        [MaxLength(200)]
        public string Author { get; set; }

        public string Reference { get; set; }

        public string Notes { get; set; }

        public int UserId { get; set; }

        public string StartOfQuote()
        {
            if (this.QuoteText.Length > 60)
            {
                return this.QuoteText.Substring(0, 60) + "...";
            } else
            {
               return this.QuoteText;
            }
        }
    }
}