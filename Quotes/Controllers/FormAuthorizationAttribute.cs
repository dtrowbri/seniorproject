﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Quotes.Models;

namespace Quotes.Controllers
{
    internal class FormAuthorizationAttribute : FilterAttribute, IAuthorizationFilter
    {
        public void OnAuthorization(AuthorizationContext filterContext)
        {
            if ((UserModel)filterContext.HttpContext.Session["user"] == null)
            {
                filterContext.Result = new RedirectResult("/user/Login");
            }
        }
    }
}