﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Quotes.Models;
using Quotes.Business.DAO;

namespace Quotes.Controllers
{
    [FormAuthorization]
    public class QuoteController : Controller
    {
        // GET: Quote
        
        public ActionResult ListQuotes()
        {
            UserModel user = (UserModel)Session["user"];
            List<QuoteModel> quotes = (new QuoteDAO()).GetQuotes(user.Userid);
            if (TempData["message"] != null)
            {
                ViewBag.Message = TempData["message"].ToString();
            }

            return View(quotes);
        }

        [HttpGet]
        public ActionResult CreateQuote()
        {
            return View();
        }

        [HttpPost]
        public ActionResult CreateQuote(QuoteModel quote)
        {
            if (!ModelState.IsValid)
            {
                return View();
            }

            quote.UserId = ((UserModel)Session["user"]).Userid;
            
            QuoteDAO dao = new QuoteDAO();
            bool results = dao.CreateQuote(quote);
            if(results)
            {
                TempData["message"] = "The quote has successfully been created.";
                return RedirectToAction("ListQuotes");
            } else
            {
                ViewBag.Message = "Failed to create quote. Please try again.";
                return View();
            }
        }

        [HttpPost]
        public ActionResult SearchQuotes(string searchString)
        {
            int userid = ((UserModel)Session["user"]).Userid;

            QuoteDAO dao = new QuoteDAO();
            List<QuoteModel> quotes = dao.SearchQuotes(userid, searchString);

            return View("ListQuotes", quotes);
        }

        [HttpPost]
        public ActionResult EditQuote(QuoteModel quote)
        {
            if (ModelState.ContainsKey("QuoteText"))
            {
                ModelState["QuoteText"].Errors.Clear();
            }
            QuoteDAO dao = new QuoteDAO();
            QuoteModel fullQuote = dao.GetQuote(quote.QuoteId);

            return View(fullQuote);
        }

        [HttpPost]
        public ActionResult EditQuoteFunction(QuoteModel quote)
        {
            if (!ModelState.IsValid)
            {
                return View("EditQuote");
            }

            QuoteDAO dao = new QuoteDAO();
            bool results = dao.EditQuote(quote);

            if (results)
            {
                TempData["message"] = "The quote has been successfully updated.";
                return RedirectToAction("ListQuotes");
            } else
            {
                ViewBag.Message = "The quote failed to update. Please try again.";
                return View("EditQuote");
            }
        }

        public ActionResult DeleteQuote(QuoteModel quote)
        {
            QuoteDAO dao = new QuoteDAO();
            bool results = dao.DeleteQuote(quote.QuoteId);

            if (results)
            {
                return RedirectToAction("ListQuotes");
            } else
            {
                TempData["message"] = "Failed to delete quote.";
                return View("ListQuotes");
            }
        }

        public ActionResult ViewQuote(QuoteModel quote)
        {
            QuoteDAO dao = new QuoteDAO();
            QuoteModel fullQuote = dao.GetQuote(quote.QuoteId);

            return View(fullQuote);
        }
    }
}