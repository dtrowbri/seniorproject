﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Quotes.Models;
using Quotes.Business.DAO;
using System.Security.Cryptography;
using System.Text;

namespace Quotes.Controllers
{
    public class UserController : Controller
    {

        [HttpGet]
        public ActionResult Registration()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Registration(UserModel user)
        {
            if (!ModelState.IsValid)
            {
                return View();
            }

            UserDAO dao = new UserDAO();

            user.Password = hashPassword(user.Password);


            if (!dao.DoesUsernameExist(user.Username))
            {
                bool results = dao.CreateUser(user);
                ViewBag.Message = "User was successfully created.";

                return RedirectToAction("Login");
            }
            else
            {
                ViewBag.Message = "Username " + user.Username + " has already been taken.";
                return View();
            }
        }

        [HttpGet]
        public ActionResult Login()
        {
            if (TempData["message"] != null)
            {
                ViewBag.Message = TempData["message"].ToString();
            }

            return View();
        }

        [HttpPost]
        public ActionResult Login(AuthenticationModel user)
        {
            if (!ModelState.IsValid)
            {
                return View();
            }

            user.Password = hashPassword(user.Password);

            UserDAO dao = new UserDAO();
            UserModel authenticatedUser = dao.Authenticate(user);

            if(authenticatedUser.Username == null)
            {
                ViewBag.Message = "The username or password is incorrect!";

                return View();
            }

            Session["user"] = authenticatedUser;

            return RedirectToAction("ListQuotes", "Quote");
        }

        [NonAction]
        private string hashPassword(string password)
        {
            SHA512CryptoServiceProvider sha512 = new SHA512CryptoServiceProvider();
            byte[] password_bytes = Encoding.ASCII.GetBytes(password);
            byte[] enc_password = sha512.ComputeHash(password_bytes);
            return Convert.ToBase64String(enc_password);
        }

        public ActionResult Logout()
        {
            Session.Clear();
            return RedirectToAction("Login", "User");
        }
    }
}