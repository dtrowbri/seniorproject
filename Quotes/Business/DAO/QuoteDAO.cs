﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Configuration;
using System.Data.SqlClient;
using Quotes.Business;
using Quotes.Models;

namespace Quotes.Business.DAO
{
    public class QuoteDAO
    {
        string ConnStr = ConfigurationManager.ConnectionStrings["Quotes_Connection_String"].ConnectionString;

        string CreateQuoteQuery = "insert into Quotes (Author, Reference, Quote, Notes, UserId) values (@Author, @Reference, @Quote, @Notes, @UserId)";
        string GetQuoteQuery = "select QuoteId, Quote, Notes, Author, Reference, UserId from Quotes where QuoteId = @QuoteId";
        string GetUserQuotesQuery = "select QuoteId, Quote, Notes, Author, Reference, UserId from Quotes where UserId = @UserId";
        string SearchQuotesQuery = "select QuoteId, Quote, Notes, Author, Reference, UserId from Quotes where UserId = @UserId " +
            "and (Quote like @SearchStr or Notes like @SearchStr or Reference like @SearchStr or Author like @SearchStr);";
        string EditQuoteQuery = "update Quotes set Quote = @Quote, Author = @Author, Reference = @Reference, Notes = @Notes where QuoteId = @Quoteid";
        string DeleteQuoteQuery = "delete from Quotes where QuoteId = @QuoteId";


        public bool CreateQuote(QuoteModel quote)
        {
            using(SqlConnection conn = new SqlConnection(ConnStr))
            {
                using(SqlCommand cmd = new SqlCommand(CreateQuoteQuery, conn))
                {
                    bool succeeded = false;
                    if (quote.Author != null)
                    {
                        cmd.Parameters.AddWithValue("@Author", quote.Author);
                    } else
                    {
                        cmd.Parameters.AddWithValue("@Author", "");
                    }
                    if (quote.Reference != null)
                    {
                        cmd.Parameters.AddWithValue("@Reference", quote.Reference);
                    } else
                    {
                        cmd.Parameters.AddWithValue("@Reference", "");
                    }
                    cmd.Parameters.AddWithValue("@Quote", quote.QuoteText);
                    if (quote.Notes != null)
                    {
                        cmd.Parameters.AddWithValue("@Notes", quote.Notes);
                    } else
                    {
                        cmd.Parameters.AddWithValue("@Notes", "");
                    }
                    cmd.Parameters.AddWithValue("@UserId", quote.UserId);

                    try
                    {
                        conn.Open();
                        int rowsAffected = cmd.ExecuteNonQuery();

                        if(rowsAffected == 1)
                        {
                            succeeded = true;
                        }
                    } catch(SqlException ex)
                    {
                        Console.Error.WriteLine(ex.StackTrace);
                    } catch(Exception ex)
                    {
                        Console.Error.WriteLine(ex.StackTrace);
                    } finally
                    {
                        conn.Close();
                    }
                    return succeeded;
                }
            }
        }

        public QuoteModel GetQuote(int QuoteId)
        {
            using (SqlConnection conn = new SqlConnection(ConnStr))
            {
                using (SqlCommand cmd = new SqlCommand(GetQuoteQuery, conn))
                {
                    cmd.Parameters.AddWithValue("@QuoteId", QuoteId);
                    QuoteModel quote = new QuoteModel();

                    try
                    {
                        conn.Open();
                        SqlDataReader reader = cmd.ExecuteReader();



                        if (reader.Read())
                        {
                            quote.QuoteId = Convert.ToInt32(reader["QuoteId"]);
                            quote.Author = reader["Author"].ToString();
                            quote.QuoteText = reader["Quote"].ToString();
                            quote.Notes = reader["Notes"].ToString();
                            quote.Reference = reader["Reference"].ToString();
                            quote.UserId = Convert.ToInt32(reader["UserId"]);
                        }
                    }
                    catch (SqlException ex)
                    {
                        Console.Error.WriteLine(ex.StackTrace);
                    }
                    catch (Exception ex)
                    {
                        Console.Error.WriteLine(ex.StackTrace);
                    }
                    finally
                    {
                        conn.Close();
                    }
                    return quote;
                }
            }
        }

        public List<QuoteModel> GetQuotes(int userid)
        {
            using (SqlConnection conn = new SqlConnection(ConnStr))
            {
                using (SqlCommand cmd = new SqlCommand(GetUserQuotesQuery, conn))
                {
                    cmd.Parameters.AddWithValue("@UserId", userid);
                    List<QuoteModel> quotes = new List<QuoteModel>();

                    try
                    {
                        conn.Open();
                        SqlDataReader reader = cmd.ExecuteReader();

                        while(reader.Read())
                        {
                            quotes.Add(
                                new QuoteModel() {
                                    QuoteId = Convert.ToInt32(reader["QuoteId"]),
                                    Author = reader["Author"].ToString(),
                                    QuoteText = reader["Quote"].ToString(),
                                    Notes = reader["Notes"].ToString(),
                                    Reference = reader["Reference"].ToString(),
                                    UserId = Convert.ToInt32(reader["UserId"])
                                });
                        }
                    }
                    catch (SqlException ex)
                    {
                        Console.Error.WriteLine(ex.StackTrace);
                    }
                    catch (Exception ex)
                    {
                        Console.Error.WriteLine(ex.StackTrace);
                    }
                    finally
                    {
                        conn.Close();
                    }
                    return quotes;
                }
            }
        }

        public List<QuoteModel> SearchQuotes(int userid, string searchStr)
        {
            using (SqlConnection conn = new SqlConnection(ConnStr))
            {
                using (SqlCommand cmd = new SqlCommand(SearchQuotesQuery, conn))
                {
                    cmd.Parameters.AddWithValue("@UserId", userid);
                    cmd.Parameters.AddWithValue("@SearchStr", "%" + searchStr + "%");
                    List<QuoteModel> quotes = new List<QuoteModel>();

                    try
                    {
                        conn.Open();
                        SqlDataReader reader = cmd.ExecuteReader();

                        while (reader.Read())
                        {
                            quotes.Add(
                                new QuoteModel()
                                {
                                    QuoteId = Convert.ToInt32(reader["QuoteId"]),
                                    Author = reader["Author"].ToString(),
                                    QuoteText = reader["Quote"].ToString(),
                                    Notes = reader["Notes"].ToString(),
                                    Reference = reader["Reference"].ToString(),
                                    UserId = Convert.ToInt32(reader["UserId"])
                                });
                        }
                    }
                    catch (SqlException ex)
                    {
                        Console.Error.WriteLine(ex.StackTrace);
                    }
                    catch (Exception ex)
                    {
                        Console.Error.WriteLine(ex.StackTrace);
                    }
                    finally
                    {
                        conn.Close();
                    }
                    return quotes;
                }
            }
        }


        public bool EditQuote(QuoteModel quote)
        {
            using (SqlConnection conn = new SqlConnection(ConnStr))
            {
                using (SqlCommand cmd = new SqlCommand(EditQuoteQuery, conn))
                {
                    bool succeeded = false;
                    if (quote.Author != null)
                    {
                        cmd.Parameters.AddWithValue("@Author", quote.Author);
                    } else
                    {
                        cmd.Parameters.AddWithValue("@Author", "");
                    }
                    if (quote.Reference != null)
                    {
                        cmd.Parameters.AddWithValue("@Reference", quote.Reference);
                    } else
                    {
                        cmd.Parameters.AddWithValue("@Reference", "");
                    }
                    cmd.Parameters.AddWithValue("@Quote", quote.QuoteText);
                    if (quote.Notes != null)
                    {
                        cmd.Parameters.AddWithValue("@Notes", quote.Notes);
                    } else
                    {
                        cmd.Parameters.AddWithValue("@Notes", "");
                    }
                    cmd.Parameters.AddWithValue("@QuoteId", quote.QuoteId);

                    try
                    {
                        conn.Open();
                        int rowsAffected = cmd.ExecuteNonQuery();

                        if (rowsAffected == 1)
                        {
                            succeeded = true;
                        }
                    }
                    catch (SqlException ex)
                    {
                        Console.Error.WriteLine(ex.StackTrace);
                    }
                    catch (Exception ex)
                    {
                        Console.Error.WriteLine(ex.StackTrace);
                    }
                    finally
                    {
                        conn.Close();
                    }
                    return succeeded;
                }
            }
        }

        public bool DeleteQuote(int  quoteid)
        {
            using (SqlConnection conn = new SqlConnection(ConnStr))
            {
                using (SqlCommand cmd = new SqlCommand(DeleteQuoteQuery, conn))
                {
                    bool succeeded = false;
                    cmd.Parameters.AddWithValue("@QuoteId", quoteid);

                    try
                    {
                        conn.Open();
                        int rowsAffected = cmd.ExecuteNonQuery();

                        if (rowsAffected == 1)
                        {
                            succeeded = true;
                        }
                    }
                    catch (SqlException ex)
                    {
                        Console.Error.WriteLine(ex.StackTrace);
                    }
                    catch (Exception ex)
                    {
                        Console.Error.WriteLine(ex.StackTrace);
                    }
                    finally
                    {
                        conn.Close();
                    }
                    return succeeded;
                }
            }
        }
    }
}