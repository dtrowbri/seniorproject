﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Quotes.Models;
using System.Data.SqlClient;
using System.Configuration;

namespace Quotes.Business.DAO
{
    public class UserDAO
    {

        string ConnStr = ConfigurationManager.ConnectionStrings["Quotes_Connection_String"].ConnectionString;

        string GetUserQuery = "select UserId, Username, EmailAddress, FirstName, LastName, Password from users where Username = @Username";
        string CreateUserQuery = "insert into users (Username, EmailAddress, FirstName, LastName, Password) values (@Username, @EmailAddress, @FirstName, @LastName, @Password)";
        string EditUserQuery = "update users set FirstName = @FirstName, LastName = @LastName, Password = @Password where userid = @UserId";
        string DeleteUserQuery = "delete from users where UserId = @UserId";
        string AuthenticateQuery = "select UserId, Username, EmailAddress, FirstName, LastName, Password from users where Password collate SQL_Latin1_General_CP1_CS_AS = @Password and UserName = @Username";
        string UsernameExistsQuery = "select count(userid) as 'Count' from users where Username = @Username";

        public UserModel getUser(string username)
        {
            using(SqlConnection conn = new SqlConnection(ConnStr))
            {

                using (SqlCommand cmd = new SqlCommand(GetUserQuery, conn))
                {
                    cmd.Parameters.AddWithValue("@Username", username);
                    UserModel user = new UserModel();
                    try
                    {
                        conn.Open();
                        SqlDataReader reader = cmd.ExecuteReader();

                        if (reader.Read())
                        {
                            user.Userid = Convert.ToInt32(reader["UserId"]);
                            user.Username = reader["Username"].ToString();
                            user.EmailAddress = reader["EmailAddress"].ToString();
                            user.FirstName = reader["FirstName"].ToString();
                            user.LastName = reader["LastName"].ToString();
                            user.Password = reader["Password"].ToString();
                        }
                    }catch (SqlException ex)
                    {
                        Console.Error.WriteLine(ex.StackTrace);
                    }catch (Exception ex)
                    {
                        Console.Error.WriteLine(ex.StackTrace);
                    }
                    finally
                    {
                        conn.Close();
                    }
                    return user;
                }
            }
        }
    
        public bool CreateUser(UserModel user)
        {
            using(SqlConnection conn = new SqlConnection(ConnStr))
            {
                using(SqlCommand cmd = new SqlCommand(CreateUserQuery, conn))
                {
                    cmd.Parameters.AddWithValue("@Username", user.Username);
                    cmd.Parameters.AddWithValue("@EmailAddress", user.EmailAddress);
                    cmd.Parameters.AddWithValue("@FirstName", user.FirstName);
                    cmd.Parameters.AddWithValue("@LastName", user.LastName);
                    cmd.Parameters.AddWithValue("@Password", user.Password);
                    bool succeeded = false;

                    try
                    {
                        conn.Open();
                        int results = cmd.ExecuteNonQuery();

                        if(results == 1)
                        {
                            succeeded = true;
                        }

                    }
                    catch (SqlException ex)
                    {
                        Console.Error.WriteLine(ex.StackTrace);
                    }
                    catch (Exception ex)
                    {
                        Console.Error.WriteLine(ex.StackTrace);
                    }
                    finally
                    {
                        conn.Close();
                    }
                    return succeeded;
                }
            }
        }
        
        public bool EditUser(UserModel user)
        {
            using (SqlConnection conn = new SqlConnection(ConnStr))
            {
                using (SqlCommand cmd = new SqlCommand(EditUserQuery, conn))
                {
                    cmd.Parameters.AddWithValue("@FirstName", user.FirstName);
                    cmd.Parameters.AddWithValue("@LastName", user.LastName);
                    cmd.Parameters.AddWithValue("@Password", user.Password);
                    cmd.Parameters.AddWithValue("@UserId", user.Userid);
                    bool succeeded = false;

                    try
                    {
                        conn.Open();
                        int results = cmd.ExecuteNonQuery();

                        if (results == 1)
                        {
                            succeeded = true;
                        }

                    }
                    catch (SqlException ex)
                    {
                        Console.Error.WriteLine(ex.StackTrace);
                    }
                    catch (Exception ex)
                    {
                        Console.Error.WriteLine(ex.StackTrace);
                    }
                    finally
                    {
                        conn.Close();
                    }
                    return succeeded;
                }
            }
        }
        
        public bool DeleteUser(int userid)
        {
            using (SqlConnection conn = new SqlConnection(ConnStr))
            {
                using (SqlCommand cmd = new SqlCommand(DeleteUserQuery, conn))
                {
                    cmd.Parameters.AddWithValue("@UserId", userid);
                    bool succeeded = false;

                    try
                    {
                        conn.Open();
                        int results = cmd.ExecuteNonQuery();

                        if (results == 1)
                        {
                            succeeded = true;
                        }

                    }
                    catch (SqlException ex)
                    {
                        Console.Error.WriteLine(ex.StackTrace);
                    }
                    catch (Exception ex)
                    {
                        Console.Error.WriteLine(ex.StackTrace);
                    }
                    finally
                    {
                        conn.Close();
                    }
                    return succeeded;
                }
            }
        }

        public UserModel Authenticate(AuthenticationModel authentication)
        {
            using (SqlConnection conn = new SqlConnection(ConnStr))
            {

                using (SqlCommand cmd = new SqlCommand(AuthenticateQuery, conn))
                {
                    cmd.Parameters.AddWithValue("@Password", authentication.Password);
                    cmd.Parameters.AddWithValue("@Username", authentication.Username);
                    UserModel user = new UserModel();
                    try
                    {
                        conn.Open();
                        SqlDataReader reader = cmd.ExecuteReader();

                        if (reader.Read())
                        {
                            user.Userid = Convert.ToInt32(reader["UserId"]);
                            user.Username = reader["Username"].ToString();
                            user.EmailAddress = reader["EmailAddress"].ToString();
                            user.FirstName = reader["FirstName"].ToString();
                            user.LastName = reader["LastName"].ToString();
                            user.Password = reader["Password"].ToString();
                        }
                    }
                    catch (SqlException ex)
                    {
                        Console.Error.WriteLine(ex.StackTrace);
                    }
                    catch (Exception ex)
                    {
                        Console.Error.WriteLine(ex.StackTrace);
                    }
                    finally
                    {
                        conn.Close();
                    }
                    return user;
                }
            }
        }
    
        public bool DoesUsernameExist(string username)
        {
            using(SqlConnection conn = new SqlConnection(ConnStr))
            {
                using(SqlCommand cmd = new SqlCommand(UsernameExistsQuery, conn))
                {
                    cmd.Parameters.AddWithValue("@Username", username);
                    bool succeeded = false;

                    try
                    {
                        conn.Open();
                        SqlDataReader reader = cmd.ExecuteReader();

                        if (reader.Read())
                        {
                            if (Convert.ToInt32(reader["Count"]) == 1)
                            {
                                succeeded = true;
                            }
                        }

                    } catch(SqlException ex)
                    {
                        Console.Error.WriteLine(ex.StackTrace);
                    } catch(Exception ex)
                    {
                        Console.Error.WriteLine(ex.StackTrace);
                    } finally
                    {
                        conn.Close();
                    }
                    return succeeded;
                }
            }
        }
    
    }
}